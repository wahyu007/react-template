import React from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import Dashboard from "views/Dashboard.js";
import Landing from "views/Landing.js";
import Profile from "views/Profile.js";
import Login from "views/Login.js";
import PrivateRoute from "auth/PrivateRoute";
import { ProvideAuth } from "auth/Context";

function App() {
  return (
    <ProvideAuth>
      <BrowserRouter>
        <Switch>
          <Route path="/dashboard" exact component={Dashboard} />
          <PrivateRoute path="/landing" component={Landing} />
          <Route path="/profile" component={Profile} />
          <Route path="/login" component={Login} />
          <Redirect from="/" to="/login" />
        </Switch>
      </BrowserRouter>
    </ProvideAuth>
  );
}

export default App;
