import React from "react";
import { Redirect, Route } from "react-router-dom";
import { useAuth } from "auth/Context";

export const PrivateRoute = ({ component: Component, ...rest }) => {
  // let auth = useAuth();
  return (
    <Route
      {...rest}
      render={({ location }) =>
        localStorage.getItem("authToken") ? (
          // auth.user ? (
          <Component />
        ) : (
          <Redirect
            to={{
              pathname: "/login",
              state: { from: location },
            }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
