import axios from "axios";

export const login = async (user) => {
  const result = await axios({
    method: "post",
    url: "https://guarded-crag-15965.herokuapp.com/api/v1/puskesmas/login",
    data: user,
  });

  return result.data;
  // console.log(user);
};
